# Shelter Database

## Install

    $ cd your/dev/directory
    $ git clone https://kvdmolen@bitbucket.org/kvdmolen/shelter-database.git

Browse to `your/dev/directory/shelter-database/index.html`

## Build CSS

Install [node-sass](https://github.com/sass/node-sass)

    $ cd your/dev/directory
    $ npm install -g node-sass

Run node-sass

    $ cd shelter-database
    $ node-sass -w scss/style.scss css/style.css


