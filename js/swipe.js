var swipe = {}
swipe.container     = document.getElementsByClassName("panes").length ? document.getElementsByClassName("panes")[0] : null
swipe.dots          = document.getElementsByClassName("dots").length ? Array.prototype.slice.call(document.getElementsByClassName("dots")[0].children, 0) : null;
swipe.panes         = Array.prototype.slice.call(swipe.container.children, 0)
swipe.containerSize = null;
swipe.currentIndex  = 0;

swipe.show = function(showIndex, percent, animate){
    showIndex = Math.max(0, Math.min(showIndex, swipe.panes.length - 1))
    percent = showIndex == 0 && percent > 0 || showIndex == swipe.panes.length - 1 && percent < 0 ? percent/3 || 0 : percent || 0
    var className = swipe.container.className
    if(animate) {
        if(className.indexOf('animate') === -1) {
            swipe.container.className += ' animate';
        }
    } else {
        if(className.indexOf('animate') !== -1) {
            swipe.container.className = className.replace('animate', '').trim()
        }
    }
    var paneIndex, pos, translate
    for (paneIndex = 0; paneIndex < swipe.panes.length; paneIndex++) {
        pos = (swipe.containerSize / 100) * (((paneIndex - showIndex) * 100) + percent)
        translate = 'translate3d(' + pos + 'px, 0, 0)';
        swipe.panes[paneIndex].style.transform = translate
        swipe.panes[paneIndex].style.mozTransform = translate
        swipe.panes[paneIndex].style.webkitTransform = translate
    }

    // update Dots
    for(var i = 0; i < swipe.dots.length; i++){
        swipe.dots[i].className = "dot"
    }
    swipe.dots[showIndex].className = "dot selected"
}

swipe.onPan = function(ev) {
    var delta = ev.deltaX;
    var percent = (100 / swipe.containerSize) * delta;
    var animate = false;
    if (ev.type == 'panend' || ev.type == 'pancancel') {
        if (Math.abs(percent) > 20 && ev.type == 'panend') {
            swipe.currentIndex += (percent < 0) ? 1 : -1;
        }
        percent = 0;
        animate = true;
    }
    swipe.show(swipe.currentIndex, percent, animate)
}

swipe.init = function(index){
    swipe.setSize()
    var mc = new Hammer(swipe.container)
    mc.on("panstart panmove panend pancancel", swipe.onPan)
    swipe.show(index)
}

swipe.setSize = function(){
    swipe.containerSize = swipe.container.offsetWidth || 1000
}

swipe.init(0)

window.onresize = function(event) {
    swipe.setSize()
}